//
//  Constants.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

struct AppConstants {
    
}

struct UrlConstants {
    static let postsUrl = "http://jsonplaceholder.typicode.com/posts"
    
    static let flickrUrl = "https://www.flickr.com/services/rest"
    
    static let flickrImage = "https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg"
    
}
