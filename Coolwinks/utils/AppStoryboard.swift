//
//  AppStoryboard.swift
//  wuIos
//
//  Created by Ankur Sharma on 02/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import UIKit

enum AppStoryboard: String{
    case main = "Main"
}

protocol ViperVCInitialiser{}


extension UIViewController: ViperVCInitialiser{}

extension ViperVCInitialiser where Self : UIViewController{
    
    static func getVc(from story: AppStoryboard)-> Self{
        let story = story.rawValue
        let storyBoard = UIStoryboard(name: story, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: String(describing: Self.self)) as! Self
        
        return vc
    }
}
