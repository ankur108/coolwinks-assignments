//
//  DataManagers.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

protocol ApiDataManagerInputProtocol: ObjectCreatorProtocol{
    var _output: ApiDataManagerOutputProtocol?{get set}
}

class ApiDataManager: ApiDataManagerInputProtocol{
    weak var _output: ApiDataManagerOutputProtocol?
    
    required init() {}
}


protocol ApiDataManagerOutputProtocol: class {
    
}

protocol LocalDataManagerInputProtocol: ObjectCreatorProtocol {}

class LocalDataManager: LocalDataManagerInputProtocol{
    required init() {}
}
