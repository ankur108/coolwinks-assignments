//
//  ViperBaseInteractor.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

protocol ViperBaseInteractorInputProtocol: ObjectCreatorProtocol{
    var _output: ViperBaseInteractorOutputProtocol? {get set}
    var _apiDataManager: ApiDataManagerInputProtocol? {get set}
    var _localDataManager: LocalDataManagerInputProtocol?{get set}
}

// base class for viper
class ViperBaseInteractor: ViperBaseInteractorInputProtocol {
    var _apiDataManager: ApiDataManagerInputProtocol?
    
    var _localDataManager: LocalDataManagerInputProtocol?
    
    weak var _output: ViperBaseInteractorOutputProtocol?
    
    required init() {}
}

protocol ViperBaseInteractorOutputProtocol: class {
    
}

