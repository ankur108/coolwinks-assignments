//
//  ViperBase.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import UIKit

protocol ViperViewProtocol: ObjectCreatorProtocol {
    var _presenter: ViperBasePresenterProtocol? {get set}
}

extension ViperViewProtocol{
    
    func showLoader(){
        Loader.instance.show()
    }
    
    func hideLoader(){
        Loader.instance.hide()
    }
    
    func showAlert(error: String){
         Utility.showAlert(error: error)
    }
    
}

extension ViperViewProtocol where Self: UIViewController{}


extension ViperViewProtocol where Self: UIViewController{}

extension ViperViewProtocol where Self: UIView{}


class BaseViewController: UIViewController,ViperViewProtocol{
    var _presenter: ViperBasePresenterProtocol?
}

class BaseTabBarViewController: UITabBarController,ViperViewProtocol{
  var _presenter: ViperBasePresenterProtocol?
}

class BaseTableViewController: UITableViewController,ViperViewProtocol{
    var _presenter: ViperBasePresenterProtocol?
}


class BaseNavigationController: UINavigationController{}
class BaseView: UIView, ViperViewProtocol {
    var _presenter: ViperBasePresenterProtocol?
}

class BaseTableView: UITableViewController, ViperViewProtocol{
    var _presenter: ViperBasePresenterProtocol?
}
