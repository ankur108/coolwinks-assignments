//
//  ViperBasePresenter.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

protocol ViperBasePresenterProtocol: ObjectCreatorProtocol{
    var _view: ViperViewProtocol?{get set}
    var _wireframe: ViperBaseWireframeProtocol? {get set}
    var _interactor: ViperBaseInteractorInputProtocol? {get set}
    var _data: ViperTransferData?{get set}
}

class ViperBasePresenter: ViperBasePresenterProtocol {
    weak var _view: ViperViewProtocol?
    var _wireframe: ViperBaseWireframeProtocol?
    var _interactor: ViperBaseInteractorInputProtocol?
    
    var _data: ViperTransferData?
    required init() {}
}


protocol ViperTransferData{}
