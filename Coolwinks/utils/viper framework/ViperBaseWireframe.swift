//
//  ViperBaseWireframe.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import UIKit

protocol ObjectCreatorProtocol: class {
    init()
}

extension ObjectCreatorProtocol{
    
    static func getSelf() -> Self {
        return Self()
    }
}


protocol ViperBaseWireframeProtocol: ObjectCreatorProtocol {
    var model: ViperModel! {get}
}

class ViperBaseWireframe: ViperBaseWireframeProtocol {
    var model: ViperModel!{
        return nil
    }
    
    required init(){}
    
    
    func setup(data: ViperTransferData? = nil) -> UIViewController? {
        guard let model = model  else {
            Debug.log(string: "model not defined")
            return nil
        }
        
        var view: ViperViewProtocol!
        
        //check if view is uiviewcontroller type
        if model.view is UIViewController.Type{
            //check for storyboard
            if let story = model.storyBoard{

                let vc = UIStoryboard(name: story.rawValue, bundle: nil).instantiateViewController(withIdentifier: String(describing: model.view))
                view = vc as? ViperViewProtocol
                
            }
        }else if model.view is UIView.Type{
            // initialise for view
        }

        //check if view exists
        guard view != nil else {
            return nil
        }
        
        let presenter = model.presenter.getSelf()
        
        let interactor = model.interactor?.getSelf()
        
        let apiManager = model.apiManager?.getSelf()
        
        let localDataManager = model.localManager?.getSelf()
        
        //view connection
        view._presenter = presenter
        
        //presenter connection
        presenter._view = view
        presenter._wireframe = self
        presenter._interactor = interactor
        
        presenter._data = data
        
        //interactor connections
        interactor?._output = presenter as? ViperBaseInteractorOutputProtocol
        interactor?._apiDataManager = apiManager
        interactor?._localDataManager  = localDataManager
        
        //api manager connections
        apiManager?._output = interactor as? ApiDataManagerOutputProtocol
        
        return view as? UIViewController
    }
    
    //navigation name convention should be
    func setupNav(with data: ViperTransferData? = nil) -> UINavigationController? {
        if let vc = setup(data: data){
            let story = model.storyBoard?.rawValue ?? ""
            let nav = UIStoryboard(name: story, bundle: nil).instantiateViewController(withIdentifier: "NavigationController") as? UINavigationController
            nav?.viewControllers = [vc]
            return nav
        }
        else{
            return nil
        }
    }
}


struct ViperModel{
    var storyBoard: AppStoryboard?
    var view: ViperViewProtocol.Type
    var presenter: ViperBasePresenterProtocol.Type
    var interactor: ViperBaseInteractorInputProtocol.Type?
    var apiManager: ApiDataManagerInputProtocol.Type?
    var localManager: LocalDataManagerInputProtocol.Type?
}
