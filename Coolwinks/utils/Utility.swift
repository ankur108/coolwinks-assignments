//
//  Utility.swift
//  wuIos
//
//  Created by Ankur Sharma on 02/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import UIKit

class Utility{
    static func showAlert(title: String = "", error: String){
        let alert = UIAlertController(title: title, message: error, preferredStyle: .alert)
        //ok action
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        getVisibleViewController()?.present(alert, animated: true, completion: nil)
    }
    
    static func getVisibleViewController( rootViewController: UIViewController? = nil) -> UIViewController? {
        var rootViewController = rootViewController
        if rootViewController == nil {
            rootViewController = UIApplication.shared.keyWindow?.rootViewController
        }
        
        if rootViewController?.presentedViewController == nil {
            return rootViewController
        }
        
        if let presented = rootViewController?.presentedViewController {
            if presented is UINavigationController {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }
            
            if presented is UITabBarController {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }
            
            return getVisibleViewController(rootViewController: presented)
        }
        return nil
    }
    
    static func fill(view: UIView, parent: UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        //left
        NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal, toItem: parent, attribute: .left, multiplier: 1.0, constant: 0.0).isActive = true
        
        
        //right
        NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal, toItem: parent, attribute: .right, multiplier: 1.0, constant: 0.0).isActive = true
        
        
        //top
        NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal, toItem: parent, attribute: .top, multiplier: 1.0, constant: 0.0).isActive = true
        
        
        //bottom
        NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: parent, attribute: .bottom, multiplier: 1.0, constant: 0.0).isActive = true
        
        parent.setNeedsLayout()
        parent.layoutIfNeeded()
    }
    
    static func add(constraint: NSLayoutConstraint.Attribute, child: UIView, parent: UIView){
        NSLayoutConstraint(item: child, attribute: constraint, relatedBy: .equal, toItem: parent, attribute: constraint, multiplier: 1.0, constant: 0.0).isActive = true
        parent.layoutIfNeeded()
    }
    
    static func add(height: CGFloat, view: UIView){
        NSLayoutConstraint.init(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant:  height).isActive = true
    }
    
    
    static func add(width: CGFloat, view: UIView){
        NSLayoutConstraint.init(item: view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant:  width).isActive = true
    }
}
