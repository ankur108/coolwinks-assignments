//
//  DebugLog.swift
//  wuIos
//
//  Created by Ankur Sharma on 01/10/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

struct Debug{
    
    static func log(string: String,file: String = #file, line: Int = #line, column: Int = #column, function: String = #function){
        #if DEBUG
        print("file",file,"function", function,"line", line, "column", column, string )
        #endif
    }
}
