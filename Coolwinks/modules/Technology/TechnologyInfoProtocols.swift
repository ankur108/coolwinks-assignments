//
//  TechnologyInfoProtocols.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/5/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.


import Foundation

import Foundation

//MARK: Wireframe -
protocol TechnologyInfoWireframeProtocol: ViperBaseWireframeProtocol {
  
}
//MARK: Presenter -
protocol TechnologyInfoPresenterProtocol: ViperBasePresenterProtocol {
  
}

extension TechnologyInfoPresenterProtocol{
  var view: TechnologyInfoViewProtocol?{
    return _view as? TechnologyInfoViewProtocol
  }
  
  var wireframe: TechnologyInfoWireframeProtocol?{
    return _wireframe as? TechnologyInfoWireframeProtocol
  }
  
//  var interactor: TechnologyInfoInteractorInputProtocol?{
//    return _interactor as? TechnologyInfoInteractorInputProtocol
//  }
  
  
  var data: Any?{
    return _data as? Any
  }
}


//MARK: Interactor -
//protocol TechnologyInfoInteractorInputProtocol: ViperBaseInteractorInputProtocol {
//  
//  
//}
//
//extension TechnologyInfoInteractorInputProtocol{
//  var output: TechnologyInfoInteractorOutputProtocol?{
//    return _output as? TechnologyInfoInteractorOutputProtocol
//  }
//  
//  var apiDataManager: ApiManagerInputProtocol?{
//    return _apiDataManager as? ApiManagerInputProtocol
//  }
//  
//}
//
//protocol TechnologyInfoInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, ApiManagerOutputProtocol{}


//MARK: View -
protocol TechnologyInfoViewProtocol: ViperViewProtocol {
  
}
extension TechnologyInfoViewProtocol{
  var presenter: TechnologyInfoPresenterProtocol?{
    return _presenter as? TechnologyInfoPresenterProtocol
  }
}

