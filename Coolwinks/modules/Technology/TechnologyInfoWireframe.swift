//
//  TechnologyInfoWireframe.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/5/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.

import UIKit

class TechnologyInfoWireframe:ViperBaseWireframe, TechnologyInfoWireframeProtocol {

  
  override var model: ViperModel!{
    let model = ViperModel(storyBoard: .main, view: TechnologyInfoViewController.self, presenter: TechnologyInfoPresenter.self, interactor: nil, apiManager: nil, localManager: nil)
    return model
  }
}
