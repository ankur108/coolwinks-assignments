//
//  PostDetailPresenter.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostDetailPresenter: ViperBasePresenter, PostDetailPresenterProtocol{
    
    func viewDidLoad() {
        guard let data = self.data else {return}
        self.view?.update(title: "User\(data.id)")
        self.view?.update(data: data.posts)
    }
}


//extension PostDetailPresenter:PostDetailInteractorOutputProtocol{
//  
//}
