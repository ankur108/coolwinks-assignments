//
//  PostDetailTableViewCell.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class PostDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    func update(data: Model){
        self.titleLabel?.text = data.title
        self.messageLabel?.text = data.message
        
        let noOfLines = data.expended ? 0 : 1
        let lineBrake: NSLineBreakMode =  data.expended ? .byWordWrapping : .byTruncatingTail
        
        self.titleLabel?.numberOfLines = noOfLines
        self.messageLabel?.numberOfLines = noOfLines
        
        self.titleLabel?.lineBreakMode = lineBrake
        self.messageLabel?.lineBreakMode = lineBrake
    }

    
    struct Model {
        let title, message: String
        var expended: Bool = false
        
        init(data: Post){
            self.title = data.title
            self.message = data.body
        }
    }
}
