//
//  PostDetailProtocols.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import Foundation

import Foundation

//MARK: Wireframe -
protocol PostDetailWireframeProtocol: ViperBaseWireframeProtocol {
  
}
//MARK: Presenter -
protocol PostDetailPresenterProtocol: ViperBasePresenterProtocol {
  func viewDidLoad()
}

extension PostDetailPresenterProtocol{
  var view: PostDetailViewProtocol?{
    return _view as? PostDetailViewProtocol
  }
  
  var wireframe: PostDetailWireframeProtocol?{
    return _wireframe as? PostDetailWireframeProtocol
  }
  
//  var interactor: PostDetailInteractorInputProtocol?{
//    return _interactor as? PostDetailInteractorInputProtocol
//  }
  
  
  var data: UserPost?{
    return _data as? UserPost
  }
}


//MARK: Interactor -
//protocol PostDetailInteractorInputProtocol: ViperBaseInteractorInputProtocol {
//
//
//}
//
//extension PostDetailInteractorInputProtocol{
//  var output: PostDetailInteractorOutputProtocol?{
//    return _output as? PostDetailInteractorOutputProtocol
//  }
//
//  var apiDataManager: ApiManagerInputProtocol?{
//    return _apiDataManager as? ApiManagerInputProtocol
//  }
//
//}

//protocol PostDetailInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, ApiManagerOutputProtocol{}


//MARK: View -
protocol PostDetailViewProtocol: ViperViewProtocol {
    func update(data: [Post])
    func update(title: String)
}
extension PostDetailViewProtocol{
  var presenter: PostDetailPresenterProtocol?{
    return _presenter as? PostDetailPresenterProtocol
  }
}

