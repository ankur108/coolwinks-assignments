//
//  PostDetailViewController.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostDetailViewController: BaseViewController{
    @IBOutlet weak var table: UITableView!
    
    var data:[PostDetailTableViewCell.Model] = []
    var cellDescriptor: String = "postDetailCell"

    var expandedRow: Int?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        self.table.tableFooterView = UIView()
        self.presenter?.viewDidLoad()
    }
}


extension PostDetailViewController: PostDetailViewProtocol{
    func update(data: [Post]) {
        self.data = data.map({PostDetailTableViewCell.Model(data: $0)})
        self.table.reloadData()
    }
    
    func update(title: String) {
        self.title = title
    }
}


extension PostDetailViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellDescriptor, for: indexPath) as! PostDetailTableViewCell
        let data = self.data[indexPath.row]
        cell.update(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var index: [IndexPath] = [indexPath]
        
        //close previous
        if let row = self.expandedRow , row != indexPath.row{
            self.data[row].expended = false
            
            let oldIndex = IndexPath(row: row, section: 0)
            index.append(oldIndex)
        }
            
        self.data[indexPath.row].expended = true
        
        expandedRow = indexPath.row
        
        tableView.beginUpdates()
        tableView.reloadRows(at: index, with: .fade)
        tableView.endUpdates()
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
