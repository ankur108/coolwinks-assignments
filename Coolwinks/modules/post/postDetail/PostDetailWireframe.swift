//
//  PostDetailWireframe.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostDetailWireframe:ViperBaseWireframe, PostDetailWireframeProtocol {

  
  override var model: ViperModel!{
    let model = ViperModel(storyBoard: .main, view: PostDetailViewController.self, presenter: PostDetailPresenter.self, interactor: nil, apiManager: nil, localManager: nil)
    return model
  }
  
  
  
}
