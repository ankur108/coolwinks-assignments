//
//  PostViewController.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostViewController: BaseViewController{
    @IBOutlet weak var table: UITableView!
    
    var data: [UserPost] = []
    var cellDescription: String = "postCell"
    
	override func viewDidLoad() {
        super.viewDidLoad()
        table.tableFooterView = UIView()
    
        self.presenter?.viewDidLoad()
    }

}


extension PostViewController: PostViewProtocol{
    func update(post: [UserPost]) {
        self.data = post
        self.table.reloadData()
    }
}

extension PostViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellDescription, for: indexPath) as! PostTableViewCell
        
        let data = self.data[indexPath.row]
        cell.update(data: PostTableViewCell.Model(data: data))
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = self.data[indexPath.row]
        self.presenter?.select(data: data)
    }
}
