//
//  PostWireframe.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostWireframe:ViperBaseWireframe, PostWireframeProtocol {

  
  override var model: ViperModel!{
    let model = ViperModel(storyBoard: .main, view: PostViewController.self, presenter: PostPresenter.self, interactor: PostInteractor.self, apiManager: PostApiDataManager.self, localManager: nil)
    return model
  }
  
  
    func showDetail(for view: PostViewProtocol?, data: UserPost) {
        guard let vc = PostDetailWireframe().setup(data: data) else{return}
        
        (view as? UIViewController)?.navigationController?.pushViewController(vc, animated: true)
    }
}
