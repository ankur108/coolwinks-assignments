//
//  PostTableViewCell.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    func update(data: Model){
        self.textLabel?.text = data.title
    }
    
    struct Model{
        let title: String
        
        init(data: UserPost){
            self.title = "User\(data.id)"
        }
    }
}
