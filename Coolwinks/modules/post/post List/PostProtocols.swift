//
//  PostProtocols.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation

import Foundation

//MARK: Wireframe -
protocol PostWireframeProtocol: ViperBaseWireframeProtocol {
    func showDetail(for view: PostViewProtocol?, data: UserPost)
}
//MARK: Presenter -
protocol PostPresenterProtocol: ViperBasePresenterProtocol {
    func viewDidLoad()
    func select(data: UserPost)
}

extension PostPresenterProtocol{
  var view: PostViewProtocol?{
    return _view as? PostViewProtocol
  }
  
  var wireframe: PostWireframeProtocol?{
    return _wireframe as? PostWireframeProtocol
  }
  
  var interactor: PostInteractorInputProtocol?{
    return _interactor as? PostInteractorInputProtocol
  }
  
  
  var data: Any?{
    return _data as? Any
  }
}


//MARK: Interactor -
protocol PostInteractorInputProtocol: ViperBaseInteractorInputProtocol {
    func getPosts(request: GetPost)
}

extension PostInteractorInputProtocol{
  var output: PostInteractorOutputProtocol?{
    return _output as? PostInteractorOutputProtocol
  }
  
  var apiDataManager: PostApiDataManagerInputProtocol?{
    return _apiDataManager as? PostApiDataManagerInputProtocol
  }
  
}

protocol PostInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, PostApiDataManagerOutputProtocol{}


//MARK: View -
protocol PostViewProtocol: ViperViewProtocol {
    func update(post: [UserPost])
}
extension PostViewProtocol{
  var presenter: PostPresenterProtocol?{
    return _presenter as? PostPresenterProtocol
  }
}

