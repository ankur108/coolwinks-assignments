//
//  PostPresenter.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class PostPresenter: ViperBasePresenter, PostPresenterProtocol{
    func viewDidLoad() {
        let request = GetPost()
        
        self.view?.showLoader()
        self.interactor?.getPosts(request: request)
    }
    
    func select(data: UserPost) {
        self.wireframe?.showDetail(for: self.view, data: data)
    }
}

extension PostPresenter:PostInteractorOutputProtocol{
    func failure(error: ApiError) {
        self.view?.hideLoader()
        self.view?.showAlert(error: error.message)
    }
    
    func success(posts: [Post]) {
        self.view?.hideLoader()
        //filter posts by userid
        let userDic = Dictionary(grouping: posts, by: {$0.userId})
        var userData:[UserPost] = []
        //create userposts
        for (key, value) in userDic{
            let data = UserPost(id: key, posts: value)
            userData.append(data)
        }
        
        self.view?.update(post: userData)
    }
}
