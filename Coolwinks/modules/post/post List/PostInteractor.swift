//
//  PostInteractor.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//


import UIKit

class PostInteractor:ViperBaseInteractor, PostInteractorInputProtocol {
    
    func getPosts(request: GetPost) {
        self.apiDataManager?.getPost(request: request)
    }
  
}

extension PostInteractor: PostApiDataManagerOutputProtocol{
    
    func success(posts: [Post]) {
        self.output?.success(posts: posts)
    }
    
    func failure(error: ApiError) {
        self.output?.failure(error: error)
    }
}
