//
//  AppNavigator.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class AppNavigator{
    static func setAppRoot(){
        guard let vc = TabBarWireframe().setup() else {return}
        guard let window = UIApplication.shared.delegate?.window else {return}
            
        window?.rootViewController = vc
        
        window?.makeKeyAndVisible()
    }
}
