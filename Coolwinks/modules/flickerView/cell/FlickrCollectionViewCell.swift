//
//  FlickrCollectionViewCell.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/5/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit
import AlamofireImage

class FlickrCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
    func update(data: Model){
        guard let url = data.url else {return}
        self.imageView.image = nil
        self.imageView.af.setImage(withURL: url)
    }
    
    struct Model{
        let url: URL?
        
        init(data: FlickrData){
            self.url = URL(string: data.url)
        }
    }
}
