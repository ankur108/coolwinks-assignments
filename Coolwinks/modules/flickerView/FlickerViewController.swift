//
//  FlickerViewController.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.


import UIKit

class FlickerViewController: BaseViewController{

    @IBOutlet weak var collectionView: UICollectionView!
    var activityIndicator: UIActivityIndicatorView!
    
    
    var data: [FlickrData] = []
    var reusableidentifier = "flickrCell"
    
    var expandedItem: Int?
    var isLoading: Bool = false
    
	override func viewDidLoad() {
        super.viewDidLoad()
    
    collectionView.delegate = self
    collectionView.dataSource = self
    
    collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "loaderView")
    
    self.presenter?.viewDidLoad()
    }
}


extension FlickerViewController: FlickerViewProtocol{
    func show(images: [FlickrData]) {
        self.data = images
        self.collectionView.reloadData()
    }
    
    func append(images: [FlickrData]) {
        data.append(contentsOf: images)
            self.activityIndicator?.stopAnimating()
            self.isLoading = false
            self.collectionView.reloadData()
    }
}


extension FlickerViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 1 {
            return isLoading ? 1 : 0
        }
        return self.data.count
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
            return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //loader
        if indexPath.section == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "loaderView", for:  indexPath)
           
            
            self.activityIndicator = UIActivityIndicatorView(style: .gray)
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            cell.contentView.addSubview(self.activityIndicator)
            Utility.fill(view: self.activityIndicator, parent: cell.contentView)
            return cell
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reusableidentifier, for: indexPath) as! FlickrCollectionViewCell
        let data = self.data[indexPath.row]
        cell.update(data: FlickrCollectionViewCell.Model(data: data))
        
        if indexPath.row == self.data.count - 1{
            self.loadMore()
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let layout = collectionViewLayout as! UICollectionViewFlowLayout
                 let space = layout.minimumInteritemSpacing + layout.sectionInset.left + layout.sectionInset.right
        
        if indexPath.section == 1{
            return CGSize(width: collectionView.bounds.size.width, height: 50)
        }
              
        if let expanded = self.expandedItem, expanded == indexPath.row{
            let size = collectionView.bounds.size
            return CGSize(width: size.width - space, height: size.height)
        }else{
            
            let width = (collectionView.bounds.width - space)/2
            return CGSize(width: width, height: width)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var index: [IndexPath] = [indexPath]
        
        //close previous
        if let item = self.expandedItem , item != indexPath.item{
            
            let oldIndex = IndexPath(item: item, section: 0)
            index.append(oldIndex)
        }
        expandedItem = indexPath.row
        
        collectionView.reloadItems(at: index)
    }
    
    
    private func loadMore(){
        guard !isLoading else {return}
        isLoading = true
        self.presenter?.loadNextPage()
        self.collectionView.reloadData()
    }
}
