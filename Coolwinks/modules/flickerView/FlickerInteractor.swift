//
//  FlickerInteractor.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.


import UIKit

class FlickerInteractor:ViperBaseInteractor, FlickerInteractorInputProtocol {

    func getImage(request: GetFlickrImage) {
        self.apiDataManager?.getImages(request: request)
    }
}

extension FlickerInteractor: FlickrApiDataManagerOutputProtocol{
    
    func failure(error: ApiError) {
        self.output?.failure(error: error)
    }
    
    func success(flickr response: FlickrImageResponse) {
        self.output?.success(flickr: response)
    }
}
