//
//  FlickerProtocols.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.

import Foundation

//MARK: Wireframe -
protocol FlickerWireframeProtocol: ViperBaseWireframeProtocol {
//    func showDetail(for view: FlickerViewProtocol?)
}
//MARK: Presenter -
protocol FlickerPresenterProtocol: ViperBasePresenterProtocol {
  func viewDidLoad()
    func loadNextPage()
}

extension FlickerPresenterProtocol{
  var view: FlickerViewProtocol?{
    return _view as? FlickerViewProtocol
  }
  
  var wireframe: FlickerWireframeProtocol?{
    return _wireframe as? FlickerWireframeProtocol
  }
  
  var interactor: FlickerInteractorInputProtocol?{
    return _interactor as? FlickerInteractorInputProtocol
  }
  
  
  var data: Any?{
    return _data as? Any
  }
}


//MARK: Interactor -
protocol FlickerInteractorInputProtocol: ViperBaseInteractorInputProtocol {
    func getImage(request: GetFlickrImage)
  
}

extension FlickerInteractorInputProtocol{
  var output: FlickerInteractorOutputProtocol?{
    return _output as? FlickerInteractorOutputProtocol
  }
  
  var apiDataManager: FlickrApiDataManagerInputProtocol?{
    return _apiDataManager as? FlickrApiDataManagerInputProtocol
  }
  
}

protocol FlickerInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, FlickrApiDataManagerOutputProtocol{}


//MARK: View -
protocol FlickerViewProtocol: ViperViewProtocol {
    func show(images:[FlickrData])
    func append(images: [FlickrData])
}
extension FlickerViewProtocol{
  var presenter: FlickerPresenterProtocol?{
    return _presenter as? FlickerPresenterProtocol
  }
}

