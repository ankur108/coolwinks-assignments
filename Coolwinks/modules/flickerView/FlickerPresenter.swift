//
//  FlickerPresenter.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.

import UIKit

class FlickerPresenter: ViperBasePresenter, FlickerPresenterProtocol{
    
    var pager: GetFlickrImage = GetFlickrImage(perPageCount: 10)
    
    func viewDidLoad() {
        self.view?.showLoader()
        self.interactor?.getImage(request: pager)
    }
    
    
    func loadNextPage() {
        if pager.upgradeForNext(){
//            self.view?.showLoader()
            self.interactor?.getImage(request: pager)
        }
    }
}


extension FlickerPresenter:FlickerInteractorOutputProtocol{
    func success(flickr response: FlickrImageResponse) {
        self.view?.hideLoader()
        
        pager.page = response.page
        pager.maxPage = response.pages
        if (response.page == 1){
            self.view?.show(images: response.photo)
        }else{
            self.view?.append(images: response.photo)
        }
    }
    
    func failure(error: ApiError) {
        self.view?.hideLoader()
        self.view?.showAlert(error: error.message)
    }
}
