//
//  FlickerWireframe.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.


import UIKit

class FlickerWireframe:ViperBaseWireframe, FlickerWireframeProtocol {

  
  override var model: ViperModel!{
    let model = ViperModel(storyBoard: .main, view: FlickerViewController.self, presenter: FlickerPresenter.self, interactor: FlickerInteractor.self, apiManager: FlickrApiDataManager.self, localManager: nil)
    return model
  }
}
