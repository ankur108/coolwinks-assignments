//
//  TabBarViewController.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class TabBarViewController: BaseTabBarViewController{

	override func viewDidLoad() {
        super.viewDidLoad()
    
    self.presenter?.viewDidLoad()
    }

}


extension TabBarViewController: TabBarViewProtocol{
  
}
