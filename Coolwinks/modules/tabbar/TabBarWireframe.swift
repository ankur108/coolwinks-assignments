//
//  TabBarWireframe.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class TabBarWireframe:ViperBaseWireframe, TabBarWireframeProtocol {

  
  override var model: ViperModel!{
    let model = ViperModel(storyBoard: .main, view: TabBarViewController.self, presenter: TabBarPresenter.self, interactor: nil, apiManager: nil, localManager: nil)
    return model
  }
    
    
    func addPost(for view: TabBarViewProtocol?) {
        guard let vc = PostWireframe().setupNav() else {return}
        
        vc.tabBarItem = UITabBarItem(title: "Posts", image: UIImage(named: "post"), tag: 0)
        
        guard let tabBar = view as? TabBarViewController else{return}
        
        var vcs = tabBar.viewControllers ?? []
        
        vcs.append(vc)
        
        tabBar.viewControllers = vcs
    }
    
    func addFlickrGallary(for view: TabBarViewProtocol?) {
        guard let vc = FlickerWireframe().setupNav() else {return}
               
               vc.tabBarItem = UITabBarItem(title: "Gallery", image: UIImage(named: "gallery"), tag: 1)
               
               guard let tabBar = view as? TabBarViewController else{return}
               
               var vcs = tabBar.viewControllers ?? []
               
               vcs.append(vc)
               
               tabBar.viewControllers = vcs
    }
    
    func addInfo(for view: TabBarViewProtocol?) {
        guard let vc = TechnologyInfoWireframe().setupNav() else {return}
               
               vc.tabBarItem = UITabBarItem(title: "Info", image: UIImage(named: "info"), tag: 2)
               
               guard let tabBar = view as? TabBarViewController else{return}
               
               var vcs = tabBar.viewControllers ?? []
               
               vcs.append(vc)
               
               tabBar.viewControllers = vcs
    }
}
