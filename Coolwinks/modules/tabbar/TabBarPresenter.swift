//
//  TabBarPresenter.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import UIKit

class TabBarPresenter: ViperBasePresenter, TabBarPresenterProtocol{
    
    func viewDidLoad() {
        self.wireframe?.addPost(for: self.view)
        self.wireframe?.addFlickrGallary(for: self.view)
        self.wireframe?.addInfo(for: self.view)
    }
}

//extension TabBarPresenter:TabBarInteractorOutputProtocol{
//
//}
