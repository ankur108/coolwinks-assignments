//
//  TabBarProtocols.swift
//  Coolwinks
//
//  Created Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation

import Foundation

//MARK: Wireframe -
protocol TabBarWireframeProtocol: ViperBaseWireframeProtocol {
    func addPost(for view: TabBarViewProtocol?)
    func addFlickrGallary(for view: TabBarViewProtocol?)
    func addInfo(for view: TabBarViewProtocol?)
  
}
//MARK: Presenter -
protocol TabBarPresenterProtocol: ViperBasePresenterProtocol {
    func viewDidLoad()
}

extension TabBarPresenterProtocol{
  var view: TabBarViewProtocol?{
    return _view as? TabBarViewProtocol
  }
  
  var wireframe: TabBarWireframeProtocol?{
    return _wireframe as? TabBarWireframeProtocol
  }
  
//  var interactor: TabBarInteractorInputProtocol?{
//    return _interactor as? TabBarInteractorInputProtocol
//  }
  
  
  var data: Any?{
    return _data as? Any
  }
}


//MARK: Interactor -
//protocol TabBarInteractorInputProtocol: ViperBaseInteractorInputProtocol {
//
//
//}
//
//extension TabBarInteractorInputProtocol{
//  var output: TabBarInteractorOutputProtocol?{
//    return _output as? TabBarInteractorOutputProtocol
//  }
//
//  var apiDataManager: ApiManagerInputProtocol?{
//    return _apiDataManager as? ApiManagerInputProtocol
//  }
//
//}
//
//protocol TabBarInteractorOutputProtocol: ViperBaseInteractorOutputProtocol, ApiManagerOutputProtocol{}


//MARK: View -
protocol TabBarViewProtocol: ViperViewProtocol {
  
}
extension TabBarViewProtocol{
  var presenter: TabBarPresenterProtocol?{
    return _presenter as? TabBarPresenterProtocol
  }
}

