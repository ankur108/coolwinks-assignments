//
//  AppResponse.swift
//  wuIos
//
//  Created by Ankur Sharma on 30/09/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation


enum ApiStatus: String, Decodable {
    case success = "true"
    case error = "false"
}

class ApiResponse<T: Decodable>: Decodable {
    var status: ApiStatus = .error
    var code: ApiErrorCode = .unknown
    var error: ApiError! {
        get{
            return ApiError(code: code, serverMessage: message, url: nil)
        }
    }
    var data: T!
    var message: String!
    
    var isSuccess: Bool {
        get{
            return status == .success && data != nil
        }
    }
    
    init(code: ApiErrorCode) {
        self.status = .error
        self.code = code
    }
    
    required init(from decoder: Decoder)throws {
        let values = try decoder.singleValueContainer()
        do {
        
            self.data = try! values.decode(T.self)
            
            self.status = .success
            self.message = "Success data"
            
        } catch let error {
            Debug.log(string:"data decoding error" +  error.localizedDescription)
            self.status = .error
            self.code = .invalid_response
        }
    }
    
}

//allow second optional type
class OptionalResponse<T: Decodable,S: Decodable>: Decodable {
  var data1:T?
  var data2:S?
  
  required init(from decoder: Decoder) throws {
    let container = try decoder.singleValueContainer()
    
    do {
      data1 = try container.decode(T.self)
    } catch {
      data2 = try container.decode(S.self)
    }
  }
}

class Empty: Decodable{}
