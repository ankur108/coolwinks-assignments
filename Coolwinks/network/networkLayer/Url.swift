//
//  Url.swift
//  wuIos
//
//  Created by Ankur Sharma on 30/09/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

protocol UrlToStringProtocol {
    var string: String{get}
}


struct Url {
    
    
    enum Post: UrlToStringProtocol{
        case get
        
        var string: String{
            switch self {
            case .get:
                return UrlConstants.postsUrl
            default:
                return ""
            }
        }
    }
    
    enum Flicker: UrlToStringProtocol{
        case getRecent
        case getPic
        
        var string: String{
            switch self {
            case .getRecent:
                return UrlConstants.flickrUrl
            default:
                return ""
            }
        }
        
        func getImageUrl(flickr data: FlickrData)-> String{
            return "https://farm\(data.farm).staticflickr.com/\(data.server)/\(data.id)_\(data.secret).jpg"
        }
    }
}
