//
//  ApiError.swift
//  wuIos
//
//  Created by Ankur Sharma on 30/09/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation

enum ApiErrorCode:Int, Error, Decodable{
    //app defined
    case unknown = 0
    case invalid_response = 1
    var desc: String{
        return String(describing: self)
    }
}


struct ApiError {
    let code: ApiErrorCode
    let serverMessage: String
    var message: String{
        return code.desc
    }
    var url: String? = nil
}
