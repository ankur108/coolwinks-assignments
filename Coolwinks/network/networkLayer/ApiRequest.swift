//
//  ApiRequest.swift
//  wuIos
//
//  Created by Ankur Sharma on 30/09/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation
import Alamofire

class ApiRequest{
    var method: HTTPMethod
    var url: String
    var header: HTTPHeaders
    var param: Parameters?
    var encoding: ParameterEncoding
    
    required internal init(url: UrlToStringProtocol,method: HTTPMethod = .get,header: [String: String]? = nil, param: [String: Any]? = nil, encoding: ParameterEncoding = URLEncoding.default ){
        self.url = url.string
        self.method = method
        self.header = HTTPHeaders(header ?? [:])
        //additional headers
        self.header["Content-Type"] = "application/json"
        self.param = param
        self.encoding = encoding
    }
    
    
    static func unsignedRequest(url: UrlToStringProtocol,method: HTTPMethod = .get,header: [String: String]? = nil, paramModel: Encodable, encoding: ParameterEncoding = URLEncoding.default )-> ApiRequest{
        let request = self.init(url: url, method: method, header: header, param: paramModel.dic, encoding: encoding)
        return request
    }
    
}


extension Encodable{
    var dic: [String: Any]?{
        if let data = try? JSONEncoder().encode(self){
            let dic = try! JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
            return dic
        }else{
            return nil
        }
    }
}
