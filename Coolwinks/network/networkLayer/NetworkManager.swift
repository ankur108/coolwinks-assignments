//
//  NetworkManager.swift
//  wuIos
//
//  Created by Ankur Sharma on 30/09/18.
//  Copyright © 2018 wu. All rights reserved.
//

import Foundation
import Alamofire

final class  NetworkManager {

    static var requestCount: Int = 0{
        didSet{
            if requestCount <= 0{
                // hide network loader
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }else{
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        }
    }
    
    
 @discardableResult
    static func create<T: Decodable>(request: ApiRequest, _ success:@escaping (ApiResponse<T>)-> (),_ failure: @escaping (ApiResponse<T>)->())-> Request{
    
    if let data = try? JSONSerialization.data(withJSONObject: request.param, options: .prettyPrinted),
        let str = String(data: data, encoding: .utf8){
    }
    
    
        let request = Session.default.request(request.url, method: request.method, parameters: request.param, encoding: request.encoding, headers: request.header)
    
    //add request
    requestCount += 1
    
    request.response { (response) in
        
        requestCount -= 1
        
        let data = response.data
        let error = response.error
        
        
        
        if let error = error{
            //error message
            Debug.log(string: error.localizedDescription)
            let errorResponse = ApiResponse<T>(code: ApiErrorCode.unknown)
            failure(errorResponse)
            
        }else if let data = data{
            
            Debug.log(string: String(data: data, encoding: .utf8)!)
            
            //data present
            if let responseData = try? JSONDecoder().decode(ApiResponse<T>.self, from: data){
             // get data
                if responseData.isSuccess{
                    success(responseData)
                }else{
                    failure(responseData)
                }
                
            }else{
                let invalidDataResponse = ApiResponse<T>(code: .invalid_response)
                failure(invalidDataResponse)
            }
        }
    }
    
        return request
    }
}
