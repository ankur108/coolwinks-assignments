//
//  FlickrApiDataManager.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation


protocol FlickrApiDataManagerInputProtocol: ApiDataManagerInputProtocol {
    func getImages(request: GetFlickrImage)
}


protocol FlickrApiDataManagerOutputProtocol: ApiDataManagerOutputProtocol {
    func failure(error: ApiError)
    func success(flickr response: FlickrImageResponse)
}

extension FlickrApiDataManagerOutputProtocol{
    func failure(error: ApiError){}
    func success(flickr response: FlickrImageResponse){}
}


class FlickrApiDataManager:ApiDataManager{}


extension FlickrApiDataManager: FlickrApiDataManagerInputProtocol{
    var flickrOutput: FlickrApiDataManagerOutputProtocol?{
        return _output as? FlickrApiDataManagerOutputProtocol
    }
    
    func getImages(request model: GetFlickrImage) {
            let request = ApiRequest.unsignedRequest(url: Url.Flicker.getRecent, method: .get, header: nil, paramModel: model)
            
            NetworkManager.create(request: request, { (response: ApiResponse<FlickrResponse>) in
                self.flickrOutput?.success(flickr: response.data.photos)
            }) { (response) in
                self.flickrOutput?.failure(error: response.error)
            }
    }
}

