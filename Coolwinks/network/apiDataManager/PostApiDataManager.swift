//
//  PostApiDataManager.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation


protocol PostApiDataManagerInputProtocol: ApiDataManagerInputProtocol {
    func getPost(request: GetPost)
}



protocol PostApiDataManagerOutputProtocol: ApiDataManagerOutputProtocol {
    func failure(error: ApiError)
    func success(posts: [Post])
}

extension PostApiDataManagerOutputProtocol{
    func failure(error: ApiError){}
    func success(posts: [Post]){}
}


class PostApiDataManager:ApiDataManager{}


extension PostApiDataManager: PostApiDataManagerInputProtocol{
    var postOutput: PostApiDataManagerOutputProtocol?{
        return _output as? PostApiDataManagerOutputProtocol
    }
    
    func getPost(request model: GetPost) {
        let request = ApiRequest.unsignedRequest(url: Url.Post.get, method: .get, header: nil, paramModel: model)
        
        NetworkManager.create(request: request, { (response: ApiResponse<[Post]>) in
            self.postOutput?.success(posts: response.data)
        }) { (response) in
            self.postOutput?.failure(error: response.error)
        }
    }
    
}
