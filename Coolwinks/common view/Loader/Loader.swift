//
//  Loader.swift
//  wuIos
//
//  Created by Ankur Sharma on 13/11/18.
//  Copyright © 2018 wu. All rights reserved.
//

import UIKit

class Loader: UIView {
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var label: UILabel!

    static let instance: Loader = {
        let bundle = Bundle(for: Loader.self)
        let nib = UINib(nibName: String(describing: Loader.self), bundle: bundle)
        let view = nib.instantiate(withOwner: nil, options: nil).first as! Loader
        
        return view
    }()
    
    func show(){
        if let vc = Utility.getVisibleViewController(){
            let view = vc.view!
            view.addSubview(self)
            
            Utility.fill(view: self, parent: view)
            activity.startAnimating()
        }
    }
    
    func hide(){
        self.activity.stopAnimating()
        self.removeFromSuperview()
    }
}
