//
//  FlickrApiEntity.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation


class GetFlickrImage: Encodable {
    let method: String = "flickr.photos.getRecent"
    let api_key: String = "e449b259146e14b0d55e770fb3577436"
    let format: String = "json"
    let nojsoncallback: Int = 1
    var page: Int = 0
    let per_page: Int
    var maxPage: Int = 1
    
    init(perPageCount: Int){
        self.per_page = perPageCount
    }
    
    func  upgradeForNext()-> Bool{
        if page + 1 < maxPage{
            page += 1
            return true
        }else{
            return false
        }
    }
}


struct FlickrResponse: Decodable {
    let photos: FlickrImageResponse
}

struct FlickrImageResponse: Decodable {
    let page, pages, perpage: Int
    let photo: [FlickrData]
}

struct FlickrData: Decodable{
    let id, secret, server: String
    let farm: Int
    var url: String{
        return Url.Flicker.getPic.getImageUrl(flickr: self)
    }
}
