//
//  PostApiEntity.swift
//  Coolwinks
//
//  Created by Ankur Sharma on 4/4/20.
//  Copyright © 2020 Ankur Sharma. All rights reserved.
//

import Foundation

struct GetPost:Encodable {}

struct Post: Decodable{
    let id,userId: Int
    let title,body: String
}


struct UserPost: ViperTransferData {
    let id: Int
    let posts: [Post]
}
